package com.silvioapps.notificationsample;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationManagerCompat;
//import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.NotificationCompat;
import android.os.Bundle;
//import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.RemoteViews;

public class MainActivity extends AppCompatActivity {
    private Button startNotification = null;
    private Button startCustomNotification = null;

    private MyNotification notification = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent intent = new Intent(this, PendingIntentActivity.class);
        notification = new MyNotification(intent,getApplicationContext());

        startNotification = (Button)findViewById(R.id.startNotification);
        startNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notification.createNotification();
                notification.startNotification();
            }
        });

        startCustomNotification = (Button)findViewById(R.id.startCustomNotification);
        startCustomNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(notification.isAndroidVersionJellyBeanOrNewer()) {
                    notification.createCustomNotification();
                    notification.startCustomNotification();
                }
            }
        });
    }
}
